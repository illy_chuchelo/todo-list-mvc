<?php


class ListsController extends Controller
{
    public function __construct( array $data = array())
    {
        parent::__construct( $data );
        $this->model = new Item();

    }

    public function edit()
    {
        if ( isset($this->params[0]) )
        {
            $this->data['list'] = $this->model->find($this->params[0]);
        } else
        {
            Router::redirect('/items/');
        }
        if ( !empty($_POST['title']))
        {
            $id = isset($_POST['id']) ? $_POST['id'] : null;
            $this->model->update($_POST, $id);

            Router::redirect('/items/');
        }

    }

    public function create()
    {

        if ($_POST && isset($_POST['title']) )
        {
            $this->model->insert($_POST);
            Router::redirect('/items/');
        }

    }

    public function delete()
    {
        if ( isset($this->params[0]) )
        {
            $this->model->delete($this->params[0]);
        }
        Router::redirect('/items/');
    }



}
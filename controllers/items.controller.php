<?php


class ItemsController extends Controller
{
    public function __construct( array $data = array())
    {
        parent::__construct( $data );
        $this->model = new Task();

    }


    public function index()
    {
        $this->data['lists'] = $this->model->getLists();
        $this->data['tasks'] = $this->model->all();
    }

    public function add()
    {
        if(!empty($_POST['name'])){
            $this->model->insert($_POST);
        }
        Router::redirect('/items/');
    }

    public function edit()
    {
        $this->data['list'] = $this->model->getLists();
        if ( isset($this->params[0]) )
        {
            $this->data['task'] = $this->model->find($this->params[0]);
        } else
        {
            Router::redirect('/items/');
        }
        if ( !empty($_POST['name']))
        {
            $id = isset($_POST['id']) ? $_POST['id'] : null;
            $this->model->update($_POST, $id);

            Router::redirect('/items/');
        }

    }

    public function delete()
    {
        if ( isset($this->params[0]) )
        {
             $this->model->delete($this->params[0]);
        }
        Router::redirect('/items/');
    }

    public function mark()
    {
        if ( isset($this->params[0]) )
        {
            $this->data['task'] = $this->model->find($this->params[0]);
        }
        if ( $this->data['task']['done'] == 0 )
        {
            $this->model->done($this->params[0]);
        }else
        $this->model->undone($this->params[0]);

        Router::redirect('/items/');

    }

    public function admin_index()
    {
        $this->data['lists'] = $this->model->getListsAll();
        $this->data['tasks'] = $this->model->all();
    }



}
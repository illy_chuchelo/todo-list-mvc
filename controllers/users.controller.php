<?php


class UsersController extends Controller
{
    public function __construct($data = array())
    {
        parent::__construct($data);
        $this->model = new User();
    }

    public function login()
    {

        if ($_POST && isset($_POST['email']) && isset($_POST['password']))
        {
            $user = $this->model->getByLogin($_POST['email']);
            $hash = md5(Config::get('salt') . $_POST['password']);
            if ($user && $user['status'] && $hash == $user['password'])
            {
                Session::set('email', $user['email']);
                Session::set('id', $user['id']);
                Session::set('role', $user['is_admin']);
                Router::redirect('/items/');
            }
            Session::setFlash('Пароль или Email неправильные!  ');

        }
    }

    public function logout()
    {
        Session::destroy();
        Router::redirect('/users/login/');
    }

    public function register()
    {

        if(!empty($_POST['email']) && !empty($_POST['password']) && !empty($_POST['repeat-password']))
        {
            if($_POST['password'] ==  $_POST['repeat-password'] )
            {
                $db_user = $this->model->getByLogin($_POST['email']);

                if ( $_POST['email'] != $db_user['email'] )
                {
                    $email              = $_POST['email'];
                    $password           = md5(Config::get('salt') . $_POST['password']);
                    $token              = md5(uniqid(rand(),1));
                    $passwordResetToken = md5(uniqid(rand(),1));
                    $message            = 'http://chuchelo.zzz.com.ua/users/verify/' . $token;
                    $headers            = 'From: illy@chuchelo.zzz.com.ua' . "\r\n";

                    $data = [
                        'email'                => $email,
                        'password'             => $password,
                        'token'                => $token,
                        'password_reset_token' => $passwordResetToken
                    ];
                    $this->model->insert($data);

                    mail($email, 'Потверждение регистрации на сайте ', $message, $headers );

                    Session::setFlash('Потвердите регистрацию на почте ' . $message);
                }else
                {
                    Session::setFlash('Такой Email уже зареган! ');
                }

            }else
            {
                Session::setFlash('Пароли не совпадают ');
            }

        }

    }

    public function verify()
    {
        if ( isset($this->params[0]) )
        {
            $db_user = $this->model->getByToken($this->params[0]);
            if( $db_user )
            {
                $token = $this->params[0];

                $this->model->setVerify($token);
                Router::redirect('/users/verify/');
            }else
            {
                Router::redirect('/404/');
            }
        }
    }

    public function forgot_password()
    {
        if(!empty($_POST['email']) )
        {
            $db_user = $this->model->getByLogin($_POST['email']);
            if ( $_POST['email'] == $db_user['email'] )
            {
                $email              = $_POST['email'];
                $passwordResetToken = $db_user['password_reset_token'];
                print_r($passwordResetToken);
                $message            = 'http://chuchelo.zzz.com.ua/users/password_reset/' . $passwordResetToken;
                $headers            = 'From: illy@chuchelo.zzz.com.ua' . "\r\n";

                mail($email, 'Вастоновление пароля', $message, $headers );

                Session::setFlash('Потвердите вастоновление на почте ');
            }else
                {
                    Session::setFlash('Email не верный ');
                }
        }


    }

    public function password_reset()
    {
        if ( isset($this->params[0]) )
        {
            $db_user = $this->model->getByTokenPass($this->params[0]);

            if( $db_user )
            {
                if(!empty($_POST['password']) && !empty($_POST['repeat-password']))
                {
                    $password  = md5(Config::get('salt') . $_POST['password']);
                    $passwordResetToken = md5(uniqid(rand(),1));
                    $data = [
                        'password'             => $password,
                        'password_reset_token' => $passwordResetToken
                    ];
                    $this->model->update($data, $db_user['id']);
                    Session::setFlash('Пароль изменен');
                }else
                    {
                        Session::setFlash('Пароли не совпадают');
                    }

            }else
            {
                Router::redirect('/404/');
            }
        }
    }



    public function admin_list(){

        $this->model = new User();
        $this->data['users'] = $this->model->all();

    }

    public function toggleStatus()
    {
        if ( isset($this->params[0]) )
        {
            $this->data['user'] = $this->model->find($this->params[0]);
        }
        if ( $this->data['user']['status'] == 0 )
        {
            $this->model->activate($this->params[0]);
        }else
            $this->model->deactivate($this->params[0]);

        Router::redirect('/admin/users/list/');

    }

    public function toggleAdmin()
    {
        if ( isset($this->params[0]) )
        {
            $this->data['user'] = $this->model->find($this->params[0]);
        }
        if ( $this->data['user']['is_admin'] == 0 )
        {
            $this->model->setAdmin($this->params[0]);
        }else
            $this->model->nonAdmin($this->params[0]);

        Router::redirect('/admin/users/list/');

    }
}

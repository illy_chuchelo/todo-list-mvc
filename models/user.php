<?php

class User extends Model
{
    protected $table = "users";

    public function getByLogin($email)
    {

        $sql = "select * from users where email = '{$email}' limit 1";

        $result = $this->database->query($sql);

        if ( isset($result[0]) )
        {
            return $result[0];
        }
        return false;
    }

    public function getByToken( $token )
    {
        $sql = "SELECT * FROM " . $this->getTable();
        $sql .= ' WHERE token = ' . "'". $token . "'";
        //  var_dump($sql);die();
        return $this->database->query( $sql )[0];
    }

    public function getByTokenPass( $token )
    {
        $sql = "SELECT * FROM " . $this->getTable();
        $sql .= ' WHERE password_reset_token = ' . "'". $token . "'";
        //  var_dump($sql);die();
        return $this->database->query( $sql )[0];
    }

    public function setVerify($token)
    {
        $sql = "UPDATE " . $this->getTable();
        $sql .= ' SET status = ' . 1;
        $sql .= ', token = ' . 'NULL';
        $sql .= ' WHERE token = ' . "'". $token . "'" ;
    //   var_dump($sql);die();
        return $this->database->query($sql);
    }

    public function activate($id)
{
    $sql = "UPDATE " . $this->getTable();
    $sql .= ' SET status = ' . 1;
    $sql .= ' WHERE id = ' . $id;
//		var_dump($sql);die();
    return $this->database->query($sql);
}

    public function deactivate($id)
    {
        $sql = "UPDATE " . $this->getTable();
        $sql .= ' SET status = ' . 0;
        $sql .= ' WHERE id = ' . $id;
//		var_dump($sql);die();
        return $this->database->query($sql);
    }

    public function setAdmin($id)
    {
        $sql = "UPDATE " . $this->getTable();
        $sql .= ' SET is_admin = ' . 1;
        $sql .= ' WHERE id = ' . $id;
//		var_dump($sql);die();
        return $this->database->query($sql);
    }

    public function nonAdmin($id)
    {
        $sql = "UPDATE " . $this->getTable();
        $sql .= ' SET is_admin = ' . 0;
        $sql .= ' WHERE id = ' . $id;
//		var_dump($sql);die();
        return $this->database->query($sql);
    }





}
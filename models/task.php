<?php


class Task extends Model
{
    protected $table = "tasks";


    public function done($id){

        $sql = "UPDATE " . $this->getTable();
        $sql .= ' SET done = ' . 1;
        $sql .= ' WHERE id = ' . $id;
//		var_dump($sql);die();
        return $this->database->query($sql);

    }

    public function undone($id){

        $sql = "UPDATE " . $this->getTable();
        $sql .= ' SET done = ' . 0;
        $sql .= ' WHERE id = ' . $id;
//		var_dump($sql);die();
        return $this->database->query($sql);
    }

    public function getLists()
    {
        $sql = "SELECT * FROM  lists WHERE user_id = " . Session::get('id');
        return $this->database->query( $sql );
    }

    public function getListsAll()
    {
        $sql = "SELECT * FROM  lists ";
        return $this->database->query( $sql );
    }

    public function delete($id){

        $sql = "delete from " . $this->getTable();
        $sql .= ' WHERE id = ' . $id;
        return $this->database->query($sql);
    }


}